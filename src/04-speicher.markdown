Speicher
========

Die Speicherverwaltung in Java ist ein wenig angesprochenes Thema, schlicht und ergreifend weil man in Java den Luxus genießt nicht über die Speicherverwaltung nachdenken zu müssen. Das bedeutet nicht dass man gar nicht über Speicher nachdenken sollte, aber es erlaubt einem doch sehr schnell zu entwickeln ohne dabei wirklich Speicherlecks zu öffnen.


Garbage Collection
------------------

In Java wird Speicher nach dem "[Garbage Collection](https://de.wikipedia.org/wiki/Garbage_Collection)" Prinzip arbeitet. Dies ist vergleichbar mit der [Referenzzählung](https://de.wikipedia.org/wiki/Referenzz%C3%A4hlung) um Speicher zu verwalten, aber der Benutzer muss sich nicht darum kümmern.

### Manuelle Speicherverwaltung

Zuerst sehen wir uns die klassische, manuelle Speicherverwaltung an wie sie zum Beispiel in C verwendet wird. Hierfür nehmen wir folgendes Beispiel:

```c
int* data[10];

for (int index = 0; index < 10; index++) {
	data[index] = malloc(sizeof(int) * 50 * 1024 * 1024);
}

// Verwendung

for (int index = 0; index < 10; index++) {
	free(data[index]);
}
```

Wir legen ein Array an mit 10 Einträgen welche jeweils 50MB in Größe sind, wir allokieren also um die 500MB. Dieser Speicher wird von uns direkt vom Betriebssystem allokiert mittels des Aufrufs von `malloc`. Nach der Verwendung geben wir diesen Speicher wieder frei, also dem Betriebssystem zurück, durch den Aufruf von `free`.

Der Speicherverlauf sieht in diesem Fall in etwa so aus:

![](./images/04-speicher/01-c-beispiel-1.svg)

Der orange Verlauf ist Speicher welcher vom Betriebssystem unserem Prozess zugewiesen wurde, der blaue Verlauf ist der Speicher welcher tatsächlich durch unsere Applikation belegt wird. In diesem Fall sind diese deckungsgleich, da wir den Speicher händisch vom Betriebssystem anfordern und auch wieder diesem zurück geben wenn wir ihn nicht mehr brauchen.

In diesem zweiten Beispiel geben wir den Speicher unmittelbar nach der Verwendung wieder frei:

```c
for (int counter = 0; counter < 10; counter++) {
	int* data = malloc(sizeof(int) * 50 * 1024 * 1024);
	
	// Verwendung
	
	free(data);
}
```

Diesmal überspringen wir das Array und allokieren lediglich ~50MB direkt in der Schleife, verwenden diese und geben es direkt danach wieder frei. Damit ergibt sich ein sehr geringer Speicherverbrauch.

![](./images/04-speicher/02-c-beispiel-2.svg)

Wir sehen dass wir nur sehr wenig Speicher belegen, nämlich immer nur diese 50MB Blöcke.

### JVM Speicherverwaltung

Da wir in beziehungsweise von der JVM ausgeführt werden, können wir nicht direkt auf den Speicher des Betriebssystems zugreifen. Wir können "lediglich" Objekte erzeugen und alle Referenzen auf dieses Objekt auf `null` setzen wenn wir es nicht mehr brauchen, dies entspricht aber nicht direkt einer Freigabe des Speichers auf Betriebssystemebene. Stattdessen ist es so dass die JVM größere Blöcke an Speicher in Vorrat hält, diese mit den Objekten füllt und nach Bedarf neue Blöcke vom Betriebssystem anfordert.

Nehmen wir wieder unser erstes Beispiel aber in Java, nehmen wir an dass eine `Data` Instanz in etwa 50MB Speicher braucht:

```java
Data[] data = new Data[10];

for (int index = 0; index < 10; index++) {
	data[index] = new Data();
}

// Verwendung

for (int index = 0; index < 10; index++) {
	data[index] = null;
}
```

Dies ist relativ ident mit dem Beispiel in C, der Speicherbedarf der Applikation sieht aber leicht anders aus:

![](./images/04-speicher/03-java-beispiel-1.svg)

Wir sehen dass die JVM am Anfang bereits einen Block Speicher vom Betriebssystem allokiert hat, noch bevor wir überhaupt etwas getan haben. Anschließend haben wir Objekte erzeugt und als der Speicher der JVM nicht mehr ausreichend war um das neue Objekt aufzunehmen, wurde einer weiterer Block durch die JVM allokiert. Dies ging solange weiter bis wir fertig waren mit unserem Programm. Hier erwartet uns nun aber eine Überraschung, obwohl wir die Referenzen auf `null` gesetzt haben, wurde der Speicher nicht wieder freigegeben, weder der Speicher unserer Objekte noch jener der JVM hat sich verringert. Dies liegt daran dass in diesem Beispiel zu keinem Zeitpunkt eine Garbage Collection stattgefunden hat.

Wenn wir die Garbage Collection mit hinein nehmen, sieht der Speicherverbrauch wie folgt aus:

![](./images/04-speicher/04-java-beispiel-1-mit-gc.svg)

Die rot markierten Stellen sind die Bereiche in welchen die Garbage Collection aktiv wurde. Wir sehen dass diese immer erst dann zum Einsatz kommt wenn der Speicher knapp zu werden droht, oder knapp wird, nämlich immer dann wenn wir an der Grenze des allokierten Speichers ankommen. Als besseres Beispiel wurde hier angenommen dass die Freigaben direkt hintereinander passieren, also das alle Objekte gleichzeitig freigegeben wurden. Damit konnte die Garbage Collection alle Objekte direkt einsammeln.

Ebenfalls ist hier gut zu erkennen dass die JVM den Speicher vom Betriebssystem nicht direkt wieder freigibt wenn die Möglichkeit besteht, stattdessen wird dieser vorgehalten um weitere Spitzen abzufangen ohne erst wieder Speicher vom Betriebssystem allokieren zu müssen. Wenn der Speicherbedarf der Applikation einmal stark ansteigt, so wie hier, und dann wieder sinkt, wird die JVM den Speicher erst langsam wieder freigeben.

Sehen wir uns nun noch das zweite Beispiel in Java an:

```java
for (int counter = 0; counter < 10; counter++) {
	Data data = new Data();
	
	// Verwendung
}
```

Wir legen eine `Data` Instanz in der Schleife an und verwenden diese direkt. Dadurch dass diese innerhalb des Blocks angelegt ist, wird die Referenz automatisch verworfen nach jedem einzelnen Schleifendurchlauf. Damit ergibt sich ein ähnliches Bild wie zum C Beispiel.

![](./images/04-speicher/05-java-beispiel-2.svg)

Es werden zwei Instanzen im Speicher gehalten, diese haben zwar keine Referenzen mehr aber wurden noch nicht von der Garbage Collection eingesammelt. Beim allokieren der dritten Instanz merkt die JVM dass der Speicher knapp zu werden beginnt, und die Garbage Collection löst aus. Dadurch werden die zwei vorhergehenden Instanzen verworfen und die dritte Instanz kann erzeugt werden ohne dass die JVM einen neuen Block an Speicher vom Betriebssystem allokieren muss. Die letzten beiden Instanzen verbleiben im Speicher da es keinen Grund gibt für die Garbage Collection aktiv zu werden.

### Speicherdruck

Wenn der verfügbare Speicher knapp wird führt diese dazu dass die Garbage Collection oft aufgerufen werden muss um wieder genügend Speicher zur Verfügung zu haben, und somit wird auch die Applikation selbst oft angehalten um wieder Speicher freizugeben. Das Verhalten sieht ähnlich zu den vorherigen Beispielen aus, mit dem Unterschied dass die Allokierungen von neuen Objekten über das eingestellte Speicherlimit der JVM gehen beziehungsweise zuvor allokierte Objekte nicht freigegeben werden können:

![](./images/04-speicher/06-speicherdruck.svg)

Wir sehen dass der Speicher knapp wird und die Garbage Collection mehrfach aufgerufen wird ohne dass diese Speicher freigeben werden kann, es kann auch kein weiterer Speicher vom Betriebssystem mehr allokiert werden. Die letzte Erstellung eines Objekts führt damit unweigerlich zu einem `OutOfMemoryError` da kein freier Speicher mehr verfügbar ist.

In einer realen Applikation ist es meistens so dass nicht unmittelbar ein `OutOfMemoryError` geworfen wird, sondern die Applikation langsamer wird, da die Garbage Collection zwar einen Teil des belegten Speichers wieder freigeben kann, insgesamt der Speicher aber knapper wird mit der Zeit und somit die Garbage Collection öfter und öfter aufgerufen wird.

 > Die Garbage Collection wird nicht nur aufgerufen wenn der Speicher zu Ende ist, sondern auch wenn dieser knapp wird beziehungsweise vor einem allokieren von mehr Speicher vom System.


Referenzen
----------

Die Garbage Collection arbeitet mit Referenzen, aber nicht unmittelbar mit Referenzzählung sondern direkt mit den einzelnen Referenzen auf diese Instanz oder das Objekt. Die Referenzen bilden einen Baum, oder einen gerichteten Graphen, ausgehend von einem sogenannten "GC Root" Objekt. Dies sind spezielle Objekte welche nie von der Garbage Collection eingesammelt werden können obwohl nichts Referenzen auf diese hat. Wenn ein Objekt keine Verbindung mehr zu einem GC Root Objekt hat, dann kann dieses von der Garbage Collection eingesammelt werden. Diese Unterscheidung ist relativ wichtig, da in der klassischen Referenzzählung Kreise an Objekten ein Problem darstellen, da aber in der JVM diese als Baum oder gerichteter Graph verarbeitet werden, kann die Garbage Collection sehr wohl feststellen ob ganze Zweige oder Kreise nicht mehr zugänglich sind.

### `null` oder nicht

Damit stellt sich natürlich eine Frage: Wie kann man sicherstellen dass die Referenzen auf Instanzen und Objekte verworfen werden? Nehmen wir wieder unser Beispiel von weiter oben:

```java
Data[] data = new Data[10];

for (int index = 0; index < 10; index++) {
	data[index] = new Data();
}

// Verwendung

for (int index = 0; index < 10; index++) {
	data[index] = null;
}
```

Wir legen das Array an, befüllen dieses und setzen die Referenzen danach auf `null`. Dies diente der Anschaulichkeit des Beispiels, in der echten Welt ist es aber sehr selten notwendig ein Array auf diese Art und Weise "auszuleeren". In den meisten Fällen passiert dies ohnehin automatisch wenn der Block verlassen wird, wenn wir dies zum Beispiel in einer Funktion ausführen.

```java
public static final void performProcessing() {
	Data[] data = new Data[10];
	
	for (int index = 0; index < 10; index++) {
		data[index] = new Data();
	}
	
	// Verwendung
}
```

Beim verlassen der Funktion sind zwar die einzelnen Referenzen im Array nicht auf `null` gesetzt, aber die Referenz auf das Array selbst ist nicht mehr vorhanden, damit kann der gesamte Zweig an Objekten eingesammelt werden und wir müssen dieses nicht händisch leeren. Ähnlich verhält es sich wenn wir die Referenz vom Array auf `null` setzen würden.

```java
Data[] data = new Data[10];

for (int index = 0; index < 10; index++) {
	data[index] = new Data();
}

// Verwendung

data = null;
```

Nun ist ebenfalls die Referenz auf diesen Zweig an Objekten verworfen, und diese können von der Garbage Collection eingesammelt werden.

 > Ein explizites "`null`en" von Referenzen und Objekten sollte nur dann stattfinden wenn es notwendig ist, zum Beispiel weil man einen "leeren" Zustand wieder repräsentieren will. Viel wichtiger und einfacher ist dass man Variablen nur in den jeweiligen Blöcken anlegt, damit diese automatisch verworfen werden beim verlassen des Blockes wo sie verwendet wurden.

### `WeakReference`

Es gibt die Klasse [`WeakReference`](https://docs.oracle.com/javase/8/docs/api/java/lang/ref/WeakReference.html) welche es erlaubt nur eine "schwache" Referenz auf eine Instanz oder ein Objekt zu halten.

```java
WeakReference<Data> weakData = new WeakReference<>(new Data());

weakData.get().doSomething();
```

Das bedeutet dass diese Referenz nur solange lebt bis die Garbage Collection diese einsammelt. Dies braucht man primär dann wenn man eine Instanz hat welche lediglich als flüchtiger Zwischenspeicher dient, und man kein Problem damit hat wenn diese verloren geht. Daher muss man vor der Verwendung sich immer eine "harte" Referenz anlegen:


```java
WeakReference<Data> weakData = new WeakReference<>(new Data());

Data data = weakData.get();

if (data != null) {
	data.doSomething();
}
```

Da es undefiniert ist wann die Garbage Collection läuft, darf man sich nie darauf verlassen ob in der `WeakReference` die Instanz noch verfügbar ist oder nicht, und muss immer zuerst eine Referenz anlegen welche verhindert dass diese Instanz oder das Objekt von der Garbage Collection eingesammelt werden kann.

### `SoftReference`

Es gibt dann noch die Klasse [`SoftReference`](https://docs.oracle.com/javase/8/docs/api/java/lang/ref/SoftReference.html) welche eine sehr ähnliche Funktion erfüllt wie die `WeakReference`. Während die `WeakReference` bei jedem Durchlauf der Garbage Collection eingesammelt wird, wird versucht eine `SoftReference` solange im Speicher zu halten bis der Speicher wirklich benötigt wird. Eine `SoftReference` lebt also länger als eine `WeakReference` und wird erst freigegeben wenn alle anderen Möglichkeiten den notwendigen Speicher zu gewinnen erschöpft sind.

Sie eignet sich daher sehr gut für längerlebigen Zwischenspeicher welche nicht sofort wieder entleert werden sollen außer wenn es notwendig ist. Die Verwendung ist hierbei ident zu sehen zur `WeakReference`.


Garbage Collector
-----------------

Es gibt [mehrere unterschiedliche Garbage Collectors für Java](https://docs.oracle.com/javase/9/gctuning/available-collectors.htm), alle funktionieren nach dem gleichen Prinzip wie oben beschrieben, aber die exakte Funktionsweise unterscheidet sich teilweise sehr stark.

Eines haben sie alle gemeinsam, um den Speicher von Objekten aufräumen zu können, müssen sie alle die Applikations-Threads anhalten. Das bedeutet dass die Applikation für einen bestimmten Zeitraum stoppt und nicht zur Verfügung steht. Je öfter die Garbage Collection laufen muss, desto öfter stoppt die Applikation.

Dies ist das bekannte Problem welches Java Applikationen haben wenn sie unter Speicherdruck geraten, nämlich dass die Garbage Collection sehr oft ausgeführt wird um so viel Speicher wie möglich wieder zu gewinnen. Wenn aber zu wenig Speicher zur Verfügung steht, wird quasi für jedes Megabyte die Garbage Collection aufgerufen, was dazu führt dass die Applikation langsamer und langsamer wird da sie oft angehalten wird.

Das ideale Speichermuster einer Java Applikation daher ist eine Null-Linie, also ein steter Speicherverbrauch welcher sich nicht bewegt. Da dies aber nahezu unmöglich ist zu erreichen, ist das zweitbeste Muster ist ein großes Sägezahnmuster. Also der Speicherverbrauch steigt an und an, bis zu dem Moment wo die Garbage Collection einmal läuft und den Großteil des Speichers wieder reklamieren kann, und dann geht es von vorne los. Idealerweise zieht sich dieser Kreis über mehrere Sekunden, wenn nicht sogar Minuten oder Stunden.

Um diese Garbage Collection Pausen so gering wie möglich zu halten, verwenden viele der neueren Garbage Collector extra Threads in welchem sie parallel den Speicher verwalten und quasi vorsortieren. Dies führt zu kürzeren Pausen, bedeutet aber auch eine gewisse Mehrbelastung parallel zur Applikation.


JVM Parameter
-------------

Die JVM akzeptiert zwei wichtige Parameter um den Speicher zu kontrollieren, nämlich `-Xms` und `-Xmx`, respektiv sind das für minimalen und maximalen Speicher. Wenn wir zum Beispiel eine JVM starten mit den Parametern:

    -Xms250M -Xmx750M

Dann wird diese am Anfang 250MB Speicher allokieren. Je nach Bedarf wird dann weiter allokiert bis 750MB erreicht wurde. Beides sind harte Grenzen für die JVM, also sie wird nie weniger und nie mehr Speicher haben als durch diese Parameter angegeben.


`OutOfMemoryError`
------------------

Wenn kein freier Speicher mehr innerhalb der JVM zur Verfügung steht, kein weiterer Block mehr vom Betriebssystem allokiert werden kann und die Garbage Collection auch keine freien Objekte findet, dann wird ein `OutOfMemoryError` geworfen. Dieser Fehler bedeutet nichts anderes als es keinen Speicher mehr gab um die Operation durchzuführen.

 > Hierbei ist wichtig zu beachten dass dort wo der Fehler auftritt nicht die Stelle sein muss wo der (meiste) Speicher belegt wurde.


VisualVM
--------

[VisualVM](https://visualvm.github.io/) ist ein Programm welches es erlaubt sich mit einem Java Prozess zu verbinden und diesen dann zu betrachten. Man kann sowohl Speicherverbrauch, CPU Auslastung, Threads, als auch die einzelnen erzeugten Objekte betrachten, beobachten und auch nachvollziehen. Desweiteren kann man Leistungsmessungen durchführen oder Thread- oder Speicherabbilder erzeugen oder öffnen.


Trivia
------

### `System.gc()`

Es gibt im `System`-Objekt die Funktion `gc()`. Diese erlaubt es dem Garbage Collector *vorzuschlagen* dass dieser nun etwas tun soll. Das ist absichtlich so wage formuliert, denn ob der Garbage Collector etwas tut oder nicht bei diesem Aufruf ist ein Implementierungsdetail des jeweiligen Garbage Collectors selbst

 > In einer "normalen" Applikation gibt es keinen Grund diese Funktion aufzurufen.

### Minecraft

Es gibt das viel zitierte Beispiel für Spiele in Java namens "Minecraft". Es wird immer dann herausgeholt wenn jemand zeigen will das Java für Spiele nicht geeignet ist. Tatsächlich, wie so oft in solchen Fällen, ist es ein schlechtes Beispiel dafür. Java wird in High-Performance-Trading eingesetzt, und wenn es für Börsenvorgänge im unter 1ms Bereich geeignet ist, wird es wohl auch dafür geeignet sein paar Klötze zu zeichnen, und tatsächlich ist es das auch. Minecraft hat genau das Problem mit der Garbage Collection welches oben beschrieben wurde. Es wird pro Sekunde relativ viel Abfall erzeugt welcher eingesammelt werden muss, was zu einem Stottern führt welches bei einem 16ms Zeitfenster (60 Bilder die Sekunde angenommen) natürlich merkbar wird. Es gibt viele andere positive Beispiele für Spiele welche in Java umgesetzt werden und diese Probleme einfach nicht haben.
