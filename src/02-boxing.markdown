Boxing
======

Es gibt zwei unterschiedliche Typen von "Werten" in Java, nämlich Primitive und Klassen.


Unterschiede
------------

Vier der wichtigsten Unterschiede sind:

 1. Primitive sind unveränderbar und sind keine Klassen (haben keine Methoden und so weiter).
 2. Primitive werden immer als Wert übergeben, nicht als Instanz. Primitive können nie `null` sein.
 3. Primitive und Klassen sind nicht austauschbar (sie wirken nur so durch den Compiler).
 4. Operatoren können nur mit Primitiven verwendet werden.

[Primitive sind immer die kleingeschriebenen Typen](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html), wie `int` oder `char`. Dazu gibt es dann die Klassen-Varianten wie [`Integer`](https://docs.oracle.com/javase/8/docs/api/java/lang/Integer.html) und [`Character`](https://docs.oracle.com/javase/8/docs/api/java/lang/Character.html).

```java
// Primitiver Typ
int wert = 5;

// Klasse
Integer wert = new Integer(5);
```

Da Java auf Klassen basiert, und fortgesetzt damit auf [`Object`](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html), ist die Konvertierung an vielen Stellen notwendig weil Methoden einfach keine primitiven Typen übernehmen können ohne diese explizit zu deklarieren. Wenn Reflection zum Einsatz kommt ist dies zum Beispiel der Fall.


Boxing und Autoboxing
---------------------

Ich habe gesagt dass diese nicht austauschbar sind, wieso kompiliert dann folgender Code und funktioniert auch?

```java
int wert = new Integer(5);
Integer wert = 5;
```

Die Antwort ist: [Autoboxing](https://docs.oracle.com/javase/tutorial/java/data/autoboxing.html). In dem Fall handelt es sich um eine "Hilfsfunktion" des Compilers welcher die Konvertierung automatisch vornimmt. Also der Code von oben wird in Wahrheit zu folgendem Code kompiliert:

```java
int wert = new Integer(5).intValue();
Integer wert = Integer.valueOf(5);
```

Ohne das wir etwas dazu machen müssen. Die [Umwandlung von Primitiv in Klasse und zurück (Boxing)](https://docs.oracle.com/javase/specs/jls/se7/html/jls-5.html#jls-5.1.7) übernimmt für uns der Compiler (Autoboxing).

Aber, solche Umwandlung haben immer das Potenzial eine Klasseninstanz zu erzeugen, nehmen wir zum Beispiel folgenden Code:

```java
int ergebnis = 0;

for (int zaehler = 1000; zaehler < 1500; zaehler++) {
    ergebnis = ergebnis + zaehler;
}
```

Diese Schleife addiert ganz simpel 500 `int`s. Das gleiche nochmal aber mit Klassen:

```java
Integer ergebnis = 0;

for (int zaehler = 1000; zaehler < 1500; zaehler++) {
    ergebnis = ergebnis + zaehler;
}
```

Das erzeugt zusätzlich 501 Instanzen der Klasse `Integer`, was wir so nicht bemerken weil der Compiler die Umwandlung übernimmt:

```java
Integer ergebnis = Integer.valueOf(0);

for (int zaehler = 1000; zaehler < 1500; zaehler++) {
    ergebnis = Integer.valueOf(ergebnis.intValue() + zaehler);
}
```

Wir haben in der zweiten Variante also viel mehr Klassenabfall erzeugt als mit den primitiven Typen. Das ist etwas auf das man immer achten sollte.

 > Der Wertebereich in diesem Beispiel ist bewusst gewählt da die Funktion `Integer.valueOf` die Werte zwischen -128 und +127 in einem Cache zwischenspeichert. Für diese Werte wird also pro Laufzeit nur eine Instanz erzeugt. Je nach JVM kann man die Obergrenze dieses Caches einstellen um Instanzen höherer Werte vorzuhalten.


Anwendung
---------

Insbesondere kommt die Umwandlung immer dann zu tragen wenn man eine `List`, `Set` oder eine `Map` an primitiven Werten halten will (oder jede andere Klasse mit generischen Parameter). Da diese Klassen nur Objekte unterstützen, muss man die Konvertierung in diesen Fällen immer beachten.

```java
Set<Integer> werte = new HashSet<>();

// Autoboxing
werte.add(5);

// Boxing
werte.add(Integer.valueOf(5));
```


Arrays
------

Arrays bilden einen leicht abgewandelten Fall da Arrays als Objekte behandelt werden unabhängig davon von welchem Basistyp sie sind, es spielt also keine Rolle ob Arrays aus Objekt-Typen oder primitiven Typen besteht. Daher können Arrays auch nicht boxed/unboxed werden.

```java
int[] werte = new int[0];

werte = new Integer[0]; // Kompilierungsfehler

Integer[] objektWerte = (Integer)werte; // Kompilierungsfehler
```

Ebenfalls können primitive Arrays direkt in generischen Parametern verwendet werden.

```java
Set<int[]> werte = new HashSet<>();

werte.add(new int[] { 1, 2, 3});
```


Gleichheit
----------

Wie bereits oben angesprochen gibt es einen Cache für Instanzen, dieser kann natürlich nur greifen wenn die `valueOf` Methoden verwendet werden und der Wert im entsprechenden Wertebereich liegt, wenn man eine neue Instanz erzeugt bekommt man natürlich eine neue Instanz. Daraus resultiert dass man die Klassen nicht behandeln kann wie die Primitiven wenn man diese vergleicht.

```java
int wert1 = 5000;
int wert2 = 5000;

wert1 == wert2; // true
```

Wo hingegen wenn man mit den Klassen arbeitet gelten auch die Regeln für Klassen:

```java
// Autoboxing
Integer wert1 = 5000;
Integer wert2 = 5000;

wert1 == wert2; // false
wert1.equals(wert2); // true

// Boxing
Integer wert1 = Integer.valueOf(5000);
Integer wert2 = Integer.valueOf(5000);

wert1 == wert2; // false
wert1.equals(wert2); // true

// Instanzen aus dem Cache
Integer wert1 = 100;
Integer wert2 = 100;

wert1 == wert2; // true
wert1.equals(wert2); // true
```

Vergleiche zwischen Primitiven und deren Klassenpendants werden immer zu Gunsten des Primitiven aufgelöst:

```java
Integer.valueOf(5) == 5;

// Kompiliert zu:
Integer.valueOf(5).intValue() == 5;
```


Cache
-----

Wie bereits weiter oben erwähnt kann `valueOf` die Instanz aus einem Cache liefern. Dies bedeutet dass dann kein neues Objekt erzeugt werden muss für diesen Wert. Laut [Java Language Specification](Dies [gilt aber nur für einen eingeschränkten Wertebereich und nicht alle Typen](https://docs.oracle.com/javase/specs/jls/se7/html/jls-5.html#jls-5.1.7) muss dies sogar der Fall sein für einen bestimmten Wertebereich je nach Typ:

 > If the value p being boxed is the result of evaluating a constant expression (§15.29) of type boolean, byte, char, short, int, or long, and the result is true, false, a character in the range '\u0000' to '\u007f' inclusive, or an integer in the range -128 to 127 inclusive, then let a and b be the results of any two boxing conversions of p. It is always the case that a == b.

Für `Integer` kann diese Wertebereiche für positive Werte über die Property `java.lang.Integer.IntegerCache.high` erweitert werden.

Wie man an der Spezifikation oben sieht, gibt es keinen Cache für `Float` und `Double` Werte, bei diesen liefert `valueOf` also immer eine neue Instanz der entsprechenden Klasse.


Trivia
------

In alten Java Versionen war es sogar schlimmer, `valueOf` verwendet einen Cache von Instanzen, also es gibt die Möglichkeit dass keine neue Instanz erzeugt wird wenn diese bereits im Cache existiert. Dies [gilt aber nur für einen eingeschränkten Wertebereich und nicht alle Typen](https://docs.oracle.com/javase/specs/jls/se7/html/jls-5.html#jls-5.1.7).

In alten Java Versionen wurde vom Compiler immer eine Instanz erzeugt:

```java
Integer result = new Integer(0);

for (int counter = 0; counter < 500; counter++) {
    result = new Integer(result.intValue() + counter);
}
```

Aber das wurde korrigiert irgendwo vor Java 1.5, glaube ich.


Anmerkung
---------

Ich persönlich mache immer gerne die Umwandlung händisch (man kann in den IDEs eine Warnung dafür einschalten), einfach damit ich nie den Überblick verliere wo Instanzen und Objekte erzeugt und verwendet werden und wo nicht.
