`String`s
=========

Ein String in Java ist eine Zeichenkette, zum Beispiel `"ABC"`. Es gibt aber einige Eigenheiten und Besonderheiten von diesen welche man im Hinterkopf behalten sollte.


Unveränderbare Objekte
----------------------

`String`s in Java sind unveränderbare (immutable) Objekte. Das bedeutet dass man für jede Operation welche den `String` verändert man eine neue Instanz bekommt und es gibt keine Operationen welche es erlauben direkt *dieses* Objekt zu verändern.

### `String`s sind Objekte, keine Primitiven

In Java vergleicht `==` ob es sich um die *gleiche Objekt-Instanz* handelt. Im Sonderfall der Primitiven, wird direkt der Wert verglichen:

```java
int wert1 = 5;
int wert2 = 5;

a == b; // true
```

Das liegt daran dass Primitive keine Objekte sind, sie sind ein Sonderfall und werden als reine Werte gehandhabt. `String`s hingegen sind Objekte, sie verhalten sich lediglich teilweise wie Primitive (implizite Erzeugung zum Beispiel, `+` Operator und so weiter), sind es aber nicht. Also wenn man nun zwei `String`s vergleicht, muss man explizit deren Wert vergleichen mit `.equals(Object)`:

```java
String vorgabe = "abc";
String eingabe = benutzerEingabe(); // "abc"

vorgabe == eingabe; // false
vorgabe.equals(eingabe); // true
```

 > `String`s müssen also **immer** mit `.equals(Object)` verglichen werden, außer in seltenen Sonderfällen.

### `String` Cache und Interning

Nun ergibt sich ergibt sich aber das manchmal ein Aufruf wie `String == String` funktioniert, insbesondere wenn man diese als Werte im Quellcode hat:

```java
String wert = "abc";

wert == "abc"; // true
```

Dies liegt daran dass es in der JVM einen `String` Cache gibt, alle im Quellcode bekannten `String`s werden automatisch "interned", also in den Cache geladen. Wir können auch explizit Instanzen aus diesem Cache holen, zum Beispiel:

```java
String vorgabe = "abc";
String eingabe = benutzerEingabe().intern(); // "abc"

vorgabe == eingabe; // true
vorgabe.equals(eingabe); // true
```

Während der Rückgabewert von `benutzerEingabe()` immer noch eine neue Instanz ist, gibt der Aufruf `intern()` die Instanz aus dem Cache zurück. Wenn es diesen `String` noch nicht im Cache gab, wird diese in den Cache geladen und dann retourniert.

Für nähere Details [siehe die Dokumentation von `String.intern()`](https://docs.oracle.com/javase/8/docs/api/java/lang/String.html#intern--).

 > Einfach `intern()` auf allen `String`s aufzurufen um dann mit `==` vergleichen zu können ist nicht anratsam. Es ist eine zu große Fehlerquelle, es sollte immer `.equals(Object)` verwendet werden.

### Konkatenierung

Das zusammenhängen von `String`s mit `+` ist eine Speicher- und CPU-intensive Operation. "Intensiv" ist hier nicht als "es dauert im Millisekunden-Bereich" zu verstehen, sondern "es ist definitiv langsamer als Aufrufe von Methoden". Dies liegt daran dass `String` unveränderlich ist und daher immer eine neue Instanz erzeugt werden muss.

```java
String a = userInput(); // "abc"
String b = userInput(); // "def"
String c = a + b; // "abcdef"
```

Die letzte Zeile besteht in diesem Fall aber aus drei Operationen:

 1. Allokieren des Speichers für `c` (Größe `a` plus Größe `b`)
 2. Kopieren von `a` in den Speicher `c`.
 3. Kopieren von `b` in den Speicher `c`.
 
Und hier erkennen wir bereits das Problem. Wenn wir zwei `String`s mit einer Größe von je 500MB zusammenhängen wollen, brauchen wir für die Operation 2000MB (500+500+1000) insgesamt. Hinzu kommt noch dass zweimal 500MB an Speicher kopiert werden müssen.

Nehmen wir an wir wollen einen `String` von eine Million Zeichen bauen, und das in einzelnen Zeichen pro Schritt:

```java
String result = "";

for (int counter = 0; counter < 1_000_000; counter++) {
    result = result + "a";
}
```

Mit dieser Kenntnis wissen wir, wir werden mindestens den Speicher für zwei Millionen Zeichen brauchen. Desweiteren, wird bei jedem Schleifendurchlauf eine größere Anzahl an Speicher kopiert.

```
Durchlauf | Neuer Speicher | Kopierter Speicher
        1 |              1 |                1+1
        2 |              2 |                2+1
        3 |              3 |                3+1
  ...            ...                 ...
   999999 |         999999 |           999999+1
  1000000 |        1000000 |          1000000+1
----------|----------------|-------------------
  1000000 |        2000000 |    2000000+1000000
```

Wir sehen, es kann eine sehr teure Operation werden. Insbesondere wenn viele große `String`s zusammen gehängt werden sollen.

 > Wenn viele `String`s zusammen gehängt werden sollen, in einer Schleife, sollte man einen `StringBuilder` verwenden. Bei kleinen und wenigen `String`s, zum Beispiel wenn man Vor- und Nachname zusammen hängt (`vorname + " " + nachname`) spielt es keine Rolle. Oder als einfache Faustregel "Wenn du es händisch machen kannst, ist es ganz sicher kein Flaschenhals".


`StringBuilder`
---------------

Da das Zusammenhängen von `String`s sehr teuer werden kann, gibt es den [`StringBuilder`](https://docs.oracle.com/javase/8/docs/api/java/lang/StringBuilder.html). Der `StringBuilder` ist, wie der Name vermuten lässt, ein Helfer um `String`s zu bauen.

```java
StringBuilder resultBuilder = new StringBuilder();

for (int counter = 0; counter < 1_000_000; counter++) {
    resultBuilder.append("a");
}

String result = resultBuilder.toString();
```

Intern hält sich der `StringBuilder` einen Buffer welcher vergrößert wird nach Bedarf. Also im Idealfall ist das erweitern des `StringBuilder`s nur ein Kopiervorgang, nämlich das kopieren des neuen Anhangs in den Buffer. Der Buffer wird recht dynamisch vergrößert, auch in diesem Fall muss dann einmal der gesamte Buffer kopiert werden, dies passiert aber nur "bei Bedarf" und der Buffer wird meistens größer allokiert als der neue Bedarf. Das hinzufügen bei einem `StringBuilder.append(String)` Aufruf besteht aus quasi folgenden Operationen:

 1. Prüfung ob genügend Platz im Buffer.
     1. Wenn nicht, neuen Buffer anlegen.
     2. Buffer-Inhalt in den neuen Buffer kopieren.
 2. Parameter in den Buffer kopieren.

Damit ist natürlich ein viel schnellerer Vorgang möglich.

 > Ein `StringBuilder` sollte immer dann verwendet werden, wenn große `String`s oder eine unbekannte Anzahl an `String`s zusammen gehängt wird.


`StringBuffer`
--------------

Es gibt auch noch den [`StringBuffer`](https://docs.oracle.com/javase/8/docs/api/java/lang/StringBuffer.html), er ist für alle Zwecke ident mit dem `StringBuilder`, mit dem Unterschied dass der `StringBuffer` synchronisiert ist. Also der `StringBuffer` kann in einem Szenario mit mehreren Threads verwendet werden.

 > In 99,999% der Fälle ist `StringBuilder` die Klasse der Wahl, da die Synchronisierung vom `StringBuffer` unnötig ist (und dennoch Leistung kostet).


`CharSequence`
--------------

Es gibt die Schnittstelle [`CharSequence`](https://docs.oracle.com/javase/8/docs/api/java/lang/CharSequence.html) welche eine grundlegende Schnittstelle für eine handvoll Operationen zur Verfügung stellt. Diese wird von unter anderem von `String`, `StringBuffer` und `StringBuilder` implementiert. Wenn man eine API konzipiert, sollte man sich überlegen ob man anstelle eines `String`s nicht besser eine `CharSequence` übernehmen will als Parameter, um dem Aufrufer es zu erleichtern diese API zu verwenden.


Enkodierung
-----------

Bei `String`s ist auch wichtig zu wissen, zumindest grob, wie die [Zeichenenkodierung](https://de.wikipedia.org/wiki/Zeichenkodierung) funktioniert. Im Endeffekt ist ja alles nur ein Byte-Array im Speicher des Computers und aus diesen "rohen" Daten werden dann Texte geformt. Dies passiert primär dadurch dass bestimmten Byte-Werten bestimmte Zeichen zugeordnet werden. Zum Beispiel im Falle von UTF-8 (und einigen anderen Enkodierungen) ist das Byte mit dem Wert `65` ein `A`, `33` ist ein `!`. Die Enkodierung ist quasi eine Übersetzungstabelle von Byte-Werten auf Zeichen.

Ein Byte kann aber nur 256 unterschiedliche Werte haben, es gibt viel, viel mehr Zeichen als das. In UTF-8 wird dieses Problem über so genannte "Seiten" oder "Code-Seiten" gelöst. Im Großen und Ganzen hat nur der Bereich 0-127 feste Zeichen zugeordnet, in 128-255 darüber folgen dann Steuerzeichen und reservierte Werte. Diese ersten 128 Byte-Werte decken sich mit den [ASCII](https://de.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange) Zeichensatz. Darin sind aber zum Beispiel keine Umlaute enthalten. Wenn man nun also ein `Ü` enkodieren will in UTF-8, braucht man dafür zwei Bytes, ein Steuerzeichen um auf eine andere Seite zu verweisen und dann die Position in dieser Seite. Im Falle von `Ü` ist das konkret `195` `156`.

Aufgrund der großen Auswahl an Zeichen und Seiten, kann eine solche Byte-Sequenz in UTF-8 zwischen 1 und 4 Byte lang sein. Also ein Zeichen kann auch aus bis zu 4-Byte Werten bestehen.

### Unterschiedliche Formate

Ein weiteres Problem in dieser Hinsicht sind die vielen unterschiedlichen Zeichenenkodierungen welche es gibt. Effektiv gibt es nur drei oder vier welche man tatsächlich in der Praxis beachten muss:

 1. [ASCII](https://de.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange)
 2. [UTF-8, UTF-16, UTF-32](https://de.wikipedia.org/wiki/Unicode)
 3. [ISO-8859](https://de.wikipedia.org/wiki/ISO_8859)
 4. [Windows-1252](https://de.wikipedia.org/wiki/Windows-1252)

Unicode ist mit ASCII kompatibel, also ein Text welcher mit ASCII enkodiert ist, ist auch immer ein gültiger UTF-8 Text. Bei den anderen beiden Enkodierungen sieht es schlechter aus. Sowohl ISO-8859-1 als auch Windows-1252 sind zwar ebenfalls kompatibel mit ASCII (und damit zu einem gewissen Grad mit Unicode), aber die Verwendung des Bereichs 128-255 ist unterschiedlich.

Insbesondere auf Microsoft Windows Maschinen werden Text oft als Windows-1252 gespeichert oder verarbeitet, was dann dazu führt dass diese auf anderen Maschinen (insbesondere UNIX) falsch angezeigt oder verarbeitet werden.

 > Hat man das Problem dass Sonderzeichen (zum Beispiel Umlaute) im Quellcode zwar korrekt angezeigt aber zur Laufzeit falsch sind, wird dies sehr wahrscheinlich an unterschiedlichen Enkodierungen zwischen der Quellcode-Datei und der Laufzeit liegt. Die einfachste Abhilfe kann hier sein die Quellcode-Dateien als UTF-8 zu enkodieren. Dies kann man in jeder gängigen IDE einstellen.

### Plattform Standard

Wenn man Dateien in Java einlesen will, zum Beispiel mit [`Files.readAllLines(Path)`](https://docs.oracle.com/javase/8/docs/api/java/nio/file/Files.html#readAllLines-java.nio.file.Path-) muss man sich stets bewusst sein dass die Standard-Enkodierung *für die aktuelle Plattform* verwendet wird. Man sollte daher darüber nachdenken die Enkodierung der Datei welche man liest fest anzugeben, zum Beispiel über die Überladung [`Files.readAllLines(Path, Charset)`](https://docs.oracle.com/javase/8/docs/api/java/nio/file/Files.html#readAllLines-java.nio.file.Path-java.nio.charset.Charset-).

```java
// Verwenden der Platform-spezifischen Standard-Enkodierung.
List<String> content = Files.readAllLines(filePath);

// Explizites angeben der zu verwenden Enkodierung.
List<String> content = Files.readAllLines(filePath, StandardCharSets.UTF_8);
```

Auch beim erzeugen von neuen `String`s aus `byte`-Arrays kann man die Enkodierung explizit angeben.

Die Standard-Enkodierung kann man auch über die Eigenschaft `file.encoding` global für die gesamte JVM setzen.

 > Mit [Java 18 wurde die Standard-Enkodierung für alle Plattformen auf UTF-8 gesetzt](https://openjdk.org/jeps/400).


`char` oder CodePoints?
-----------------------

In vielen Beispielen zur `String`-Verarbeitung findet man dass dieser `String` in einzelne `char`s zerlegt wird. `char` ist ein primitiver Datentyp welcher aus zwei Bytes besteht, also Werte zwischen 0 und 65535 darstellen kann. Dies ist aber nicht ausreichend um alle UTF-8 oder UTF-16 Zeichen halten zu können, in beiden Enkodierungen können Zeichen aus bis zu 4 Bytes bestehen.

Zum Beispiel das zählen aller Großbuchstaben:

```java
public static final int countUppercaseCharacters(String string) {
    int count = 0;
    
    for (char character : string.toCharArray()) {
        if (Character.isUpperCase(character)) {
            count++;
        }
    }
    
    return count;
}
```

Dies kann aber bei bestimmten Sprachen und Zeichen versagen, da immer nur 2-Byte gleichzeitig als ein Zeichen verarbeitet werden. Dieses Problem tritt nicht auf wenn man mit CodePoints arbeitet:

```java
public static final int countUppercaseCharacters(String string) {
    int count = 0;
    
    for (int codePoint : string.codePoints().toArray()) {
        if (Character.isUpperCase(codePoint)) {
            count++;
        }
    }
    
    return count;
}
```

Wenn man also den gesamten UTF-8/UTF-16 Raum abdecken will, so muss man [die "neue" CodePoint-API mit `int`-Funktionen anstelle](https://docs.oracle.com/javase/tutorial/i18n/text/design.html) der `char`-Funktionen verwenden.


Escaping
--------

Um besondere Zeichenfolgen einzuleiten wird in Java ein umgekehrter Schrägstrich/Backslash "\" verwendet, dies wird verwendet wenn man zum Beispiel ein Anführungszeichen im `String` braucht oder ein Unicode Zeichen haben will, welches man aber nicht tippen kann:

```java
String text = "Bitte gib deinen \"Namen\" ein:";

String text2 = "Unicode: \u04ef";
```

Wenn man einen umgekehrten Schrägstrich selbst will, muss man diesen mit einem zweiten umgekehrten Schrägstrich schreiben:

```java
String text3 = "Umgekehrter Schraegstrich: \\";
```


Trivia
------

### Kompilierung

Bestimmte Optimierungen werden direkt von vielen Compilern vorgenommen, zum Beispiel:

```java
String a = "abc" + "def" + "ghi";
```

Kann vom Compiler direkt umgewandelt werden in zwei Möglichkeiten:

```java
/* 1. */ String a = "abcdefghi";
/* 2. */ String a = new StringBuilder().append("abc").append("def").append("ghi").toString();
```

Wir wir sehen ist das `+` auch nur eine Hilfsfunktion des Compilers. Im Hintergrund werden `String`-Konkatenierungen immer zu einem `StringBuilder` aufgelöst.

Diese Details sind aber zu einem großen Grad Compiler abhängig und Implementierungsdetails des Compilers. Im Zweifelsfall sollte man sich dies mit einem Decompiler betrachten (aber auch hier muss man darauf achten dass der Decompiler eventuell den resultierenden Quellcode "schönt").

### UTF-16...aber nicht immer

`String`s in Java werden als UTF-16 im Speicher gehalten, also das Zeichen "a" braucht zwei Byte im Speicher. Die interne Enkodierung ist aber ein Implementierungsdetail der JVM und ist so nicht an den Programmierer kommuniziert. Deswegen gibt es in neueren JVMs auch die Optimierung dass `String`s welche nicht die volle Funktionalität von UTF-16 verwenden als einfaches Byte-Array im Speicher halten wo "a" auch nur ein Byte braucht.

Dies wurde implementiert um eben genau den Speicherdruck zu senken in dem meisten Anwendungsfällen, und ist eben nur möglich weil die interne Enkodierung und Darstellung eben nicht Teil von Java ist.

### Geteilter interner Speicher

Bestimmte Operationen auf einem `String` führen nicht unmittelbar zu einem entsprechenden Anstieg im Speicher. Zum Beispiel `String.substring(int)` liefert zwar eine neue `String` Instanz, aber das interne Array wird geteilt. Es wird also nur eine neue `String` Instanz erzeugt mit dem selben Array, aber einem anderen Offset in diesem Array.

Aber auch das ist ein Implementierungsdetail der JVM am Ende des Tages.
