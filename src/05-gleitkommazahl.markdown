Gleitkommazahlen
================

Die primitiven Datentypen in Java umfassen die beiden [Gleitkommazahlen-Typen](https://de.wikipedia.org/wiki/Gleitkommazahl) `float` und `double`. Diese werden leider immer wieder vereinfacht als "Zahlen mit Kommastellen" dargestellt und damit als Typ der Wahl wenn man etwas mit Kommastellen berechnen muss. Aber sowohl `float` als auch `double` können in den meisten Fällen keine exakten Werte abbilden, was dann zu Rundungsfehlern führt im Zuge von Berechnungen.


Allgemeines
-----------

[Die Implementierung von `float` und `double`](https://docs.oracle.com/javase/specs/jls/se20/html/jls-4.html#jls-4.2.3) entspricht dem [IEEE 754](https://de.wikipedia.org/wiki/IEEE_754) Standard, wobei `float` den Typen mit einfacher (32-Bit) und `double` dem Typen mit doppelter Genauigkeit (64-Bit) entspricht.

Neben positiven und negativen Zahlen, gibt es dann auch noch eine positive und negative 0, eine negative und eine positive Unendlichkeit als auch den Sonderwert `Not a Number`.


Ungenauigkeit
-------------

Gleitkommazahlen sind bei Definition ungenau, dies liegt daran dass man selbst in einem 64-Bit Wert nicht einfach eine nennenswerte Anzahl an Kommazahlen darstellen kann. Daher wird nur eine Annäherung an den tatsächlichen Wert gespeichert. Dies kann man bereits relativ simpel demonstrieren:

```java
float value1 = 36.2f;
float value2 = 0.362f * 100.0f;

value1 == value2; // false
```

Dieses Programm hat zwei `float` Werte, welche laut unserem Rechenverständnis ident sein müssten, dennoch gibt das Programm `false` aus. Erst bei näherer Betrachtung der Werte zeigt sich dass diese nicht ident sind:

```java
36.2f; // 36.2
0.362f * 100.0f; // 36.199997
```

Diese Ungenauigkeiten sind kein Fehler, sie sind einfach Teil dessen wie eine Gleitkommazahl nach IEEE 754 funktioniert. Wir müssen uns dieser Tatsache allerdings bewusst sein dass dem so ist.

### Ignorierbar

Dennoch werden `float` und `double` insbesondere im Grafik-Bereich sehr viel und häufig eingesetzt. Dies liegt daran dass man keine *exakte* Genauigkeit in diesem Bereich braucht. Ob der rote Pixel nun exakt auf "412x98" liegt oder auf "412.00142x98.00932" spielt keine Rolle. Auch ist es vernachlässigbar ob das Gesamtgewicht nun "8659kg" oder "8658.99999994433kg" ist.

Des Weiteren können moderne Prozessoren und auch Grafikkarten sehr gut und schnell mit `float`s und `double`s rechnen, was bedeutet dass es einfach mehr Leistung bringt diese Datentypen einzusetzen.

### Fatal

Hingegen gibt es Anwendungsgebiete in welchem diese Rundungsfehler fatal sein können. Jede Rechnung welche mit Geld zu tun hat sollte nicht mit Gleitkommazahlen gemacht werden, auch wenn das Ergebnis auf die 10 Nachkommastelle genau sein muss, sollte man vom Einsatz von Gleitkommazahlen absehen.

Diese Rundungsfehler können sich durchaus über mehrere Rechenoperationen Aufschaukeln und zu großen Abweichungen im Ergebnis führen.

### Vergleichen

Will man mehrere Gleitkommazahlen auf Gleichheit prüfen, so macht man dies am besten mit einem Epsilon-Wert welcher eine Toleranz zum Vergleich hinzufügt:

```java
float EPSILON = 0.00001f;

float value1 = 36.2f;
float value2 = 0.362f * 100.0f;

Maths.abs(value1 - value2) <= EPSILON; // true
```

Man zieht also die eine Zahl von der anderen ab und prüft dann ob das Ergebnis kleiner ist als der Epsilon-Wert.

 > Diese Toleranz muss natürlich je nach Anwendungsfall gewählt und angepasst werden.


`BigDecimal`
------------

[`BigDecimal`](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html) ist eine unveränderbare Klasse welche es erlaubt Kommazahlen in beliebiger Präzision zu halten und zu verarbeiten. Der Nachteil hiervon ist dass die Operatoren (`+`, `-`, `*` und so weiter) nicht verwendet werden können, sondern stattdessen kommen Funktionen zum Einsatz, und die Berechnungen sind relativ teurer als die von `float` oder `double`. Diesen Leistungsunterschied wird man aber erst bei wirklich vielen Berechnungen bemerken.

Das Beispiel von oben umgesetzt mit `BigDecimal`:

```java
BigDecimal value1 = new BigDecimal("36.2");
BigDecimal value2 = new BigDecimal("0.362").multiply(new BigDecimal("100"));

value1.stripTrailingZeros().equals(value2.stripTrailingZeros()); // true
```

Damit erhalten wir die gewünschte Ausgabe von `true`. Es ist definitiv wuchtiger als das `float`-Beispiel von oben, insbesondere die Aufrufe von [`stripTrailingZeros`](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html#stripTrailingZeros--) fallen auf welche notwendig sind für den [`equals`](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html#equals-java.lang.Object-) Aufruf.

 > Überladen von Operatoren (`+`, `-`, und so weiter) wurde bewusst aus Java als Sprache raus gelassen, primär um die Lesbarkeit von Code zu gewährleisten.

### Nachfolgende Nullen

Die Aufrufe auf `stripTrailingZeros` sind notwendig da ein `BigDecimal` nur dann einem anderen entspricht wenn [der Wert](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html#unscaledValue--) als auch [die Skalierung](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html#scale--) ident sind. Also ein `BigDecimal` mit dem Wert "600.0" hat einen Wert/Skalierung von `[6000, 1]`, wo hingegen der `BigDecimal` nach dem Aufruf von `stripTrailingZeros` einen Wert/Skalierung von `[6, -2]` hat. Wenn wir nun das Beispiel von oben erneut betrachten:

```java
new BigDecimal("36.2"); // [362, 1]
new BigDecimal("0.362").multiply(new BigDecimal("100")); // [36200, 3]
```

Damit sind diese beiden `BigDecimal` Instanzen nicht ident nach der Definition von `equals`.

Dies spielt bei Berechnungen keine Rolle, sondern [lediglich bei vergleichen](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html#equals-java.lang.Object-) und Ausgaben. [`compareTo`](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html#compareTo-java.math.BigDecimal-) auf der anderen Seite ignoriert unterschiedliche Werte und Skalierungen und vergleicht direkt die dargestellten Werte.

### Ungenaue Initialisierung

Ein `BigDecimal` hat keine Ungenauigkeit an sich, kann aber diese beim initialisieren von `float` und `double` Werten übernehmen. Wenn man einen `BigDecimal` mit einer Gleitkommazahl erstellt, wird der tatsächliche Wert der Gleitkommazahl übernommen:

```java
new BigDecimal(36.2f); // 36.200000762939453125
new BigDecimal(0.362f * 100.0f); // 36.1999969482421875
```

Daher sollte ein `BigDecimal` nach Möglichkeit immer [mit einem `String` initialisiert](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html#BigDecimal-java.lang.String-) werden:

```java
new BigDecimal("36.2"); // 36.2
```

 > Beim erstellen von einem `BigDecimal` aus einem `String` ist zu beachten dass das Zahlenformat fix ist nicht *nicht* der lokalen Schreibweise entspricht. Siehe [die Dokumentation für den Konstruktor](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html#BigDecimal-java.lang.String-).

### Andere `Number`s

`BigDecimal` erbt von [`Number`](https://docs.oracle.com/javase/8/docs/api/java/lang/Number.html), eine weitere wichtige Ableitung in dieser Hinsicht ist [`BigInteger`](https://docs.oracle.com/javase/8/docs/api/java/math/BigInteger.html), welche eine Implementierung einer Ganzzahl mit arbiträrer Größe ist.

 > Die maximale Größe von `BigDecimal` und `BigInteger` sind unspezifiziert und können durchaus als lediglich limitiert basierend auf dem verfügbaren Speicher angenommen werden.

### Maximale Genauigkeit und Runden

Dennoch kann man einen `BigDecimal` in der maximalen Größe beziehungsweise Genauigkeit einschränken, alle Konstruktoren und Operationen akzeptieren einen zusätzlichen [`MathContext`](https://docs.oracle.com/javase/8/docs/api/java/math/MathContext.html). Mit diesem kann man die Genauigkeit als auch die Rundungsmethode angeben sollte die eingestellte Genauigkeit überschritten werden.

Hierbei ist zu beachten dass hier mit "Genauigkeit" nicht Nachkommastellen sondern Stellen im allgemeinen gemeint sind. Also ein `BigDecimal` welcher mit dem `new MathContext(3)` erstellt wurde kann nur 3 Stellen haben, egal ob diese vor oder nach dem Komma kommen.

 > Will man einen `BigDecimal` auf zum Beispiel zwei Kommastellen runden, so kann man dies mit der `setScale` Methode tun. Entgegen der ersten Vermutung welche man haben kann ist nicht die `round(MathContext)` Methode die Methode der Wahl in diesem Fall, da man mit einem `MathContext` eben nur gesamte Stellen angeben kann.
